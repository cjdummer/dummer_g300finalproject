﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public static Music music;

    AudioSource source;
    void Awake()
    {
        
        if(music == null) //uses singleton pattern to maintain music between scenes.
        {
            music = this;
            source = GetComponent<AudioSource>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
