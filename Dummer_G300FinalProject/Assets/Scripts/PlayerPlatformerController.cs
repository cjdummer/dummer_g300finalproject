﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerPlatformerController : PhysicsObject
{

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    public Text jumpText;
    public Text loseText;
    public Text parText;
    public Text collectibleText;
    public Text fallText;
    bool jumped;

    
    
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    // Start is called before the first frame update
    void Awake()
    {
        print(GameController.gc.numCollectibles);
        //all components are retrieved, UI is set to what it should say.
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        fallText.text = "";
        loseText.text = "";
        Time.timeScale = 1;
        parText.text = "Par: " + GameController.gc.par + " Jumps Remaining";
        collectibleText.text = "Collectibles: " + GameController.gc.numCollected + " / " + GameController.gc.numCollectibles;
        
        

    }

    void Update()
    {
        if ((GameController.gc.jumpCount == 0 && grounded) || (rb2d.position.y < 0))
        {
            RestartLevel(); //brings user to a restart menu if they run out of jumps or fall
        }
        
        targetVelocity = Vector2.zero;
        ComputeVelocity();
        setCountText();
        setCollectibleText();
        quit();
    }

    void setCollectibleText()
    {
        collectibleText.text = "Collectibles: " + GameController.gc.numCollected + " / " + GameController.gc.numCollectibles; //updates the text of the collectibles when they are picked up.
    }
    void setCountText()
    {
        jumpText.text = "Jumps Remaining: " + GameController.gc.jumpCount; //updates how many jumps are remaining
    }

    protected override void ComputeVelocity() //handles player movement and jumping, as well as animations with flipping sprites and running
    {
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        jump();

        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool("Grounded", grounded);
        //animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
        animator.SetFloat("Speed", Mathf.Abs(move.x));

        targetVelocity = move * maxSpeed;
    }


    void quit()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    void jump() //handles jumping and jump animation
    {
        if (Input.GetButtonDown("Jump") && GameController.gc.jumpCount > 0)
        {
            velocity.y = jumpTakeOffSpeed;
            
            GameController.gc.jumpCount--;
            animator.SetBool("Grounded", grounded);
            jumped = true;
        }
        else if (Input.GetButtonUp("Jump") && GameController.gc.jumpCount > 0)
        {
            jumped = false;
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * .5f;
            }
        }
        animator.SetBool("Jumped", jumped);
    }

    void RestartLevel() //once player presses F after this is called, the scene resets
    {

        if (GameController.gc.jumpCount == 0 && grounded)
        {
            loseText.text = "You Have Used All Your Jumps! Press F to Restart";
        }
        else
        {
            fallText.text = "You fell! Press F to Restart";
        }
        Time.timeScale = 0;
        if (Input.GetKey(KeyCode.F))
        {
            loseText.text = "";
            
           
            SceneManager.LoadScene(GameController.currentLevel);
        }

    }

    void OnTriggerEnter2D(Collider2D collision) //this increments the counter of collectibles when the player intersects with a collectible object.
    {
        if (collision.tag == "Collectible")
        {
            GameController.gc.numCollected++;
            
        }
    }
}
