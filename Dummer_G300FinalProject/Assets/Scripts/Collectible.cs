﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectible : MonoBehaviour
{
   
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
           
            Destroy(gameObject); //When picked up, destroys itself. Rest of functionality is handled in PlayerPlatformerController
           
        }
    }
}
