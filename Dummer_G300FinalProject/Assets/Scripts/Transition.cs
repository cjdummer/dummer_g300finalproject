﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour //This handles the scene that appears between levels, summarizing how you did.
{
    public Text parText;
    public Text collectibleText;
    public Text allCollectibleText;
    public Text madeParText;

    void Start()
    {
        
        if (GameController.gc.jumpCount >= GameController.gc.par)
        {
            parText.text = "You made the level in par! Congratulations!";
            madeParText.text = "";
        }
        else
        {
            parText.text = "";
            madeParText.text = "You used a few too many jumps. Try to finish with fewer!";
        }

       if(GameController.gc.numCollected == GameController.gc.numCollectibles)
        {
            allCollectibleText.text = "";
            collectibleText.text = "You found all the collectibles!";
        }
        else
        {
            //collectibleText.rectTransform.position -= new Vector3(150, 0, 0);
            collectibleText.text = "";
            allCollectibleText.text = "You missed a few collectibles, try going back!";
        }
    }

    void Update() //user can press F to continue or G to replay the level they just completed.
    {
        if (Input.GetKey(KeyCode.F))
        {
            GameController.currentLevel++;
            SceneManager.LoadScene(GameController.currentLevel); //Because currentlevel is also indexing an array, and scene 0 is the transition scene, the next scene is always the index + 1
          //  parText.rectTransform.position += new Vector3(170, 0, 0);
          //  collectibleText.rectTransform.position += new Vector3(150, 0, 0);
        }
        if (Input.GetKey(KeyCode.G))
        {
            SceneManager.LoadScene(GameController.currentLevel); //Because currentlevel is also indexing an array, and scene 0 is the transition scene, the next scene is always the index + 1
          //  parText.rectTransform.position += new Vector3(170, 0, 0);
           // collectibleText.rectTransform.position += new Vector3(150, 0, 0);
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    
}
