﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfo //defines the LevelInfo datatype which is used to quickly define the amount of jumps, par, and collectibles in each level.
{
    public int jumps;
    public int par;
    public int collectibles;

    public LevelInfo(int j, int p, int c)
    {
        jumps = j;
        par = p;
        collectibles = c;
    }
}
