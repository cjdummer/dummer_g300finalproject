﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEnd : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") //If the player triggers the end of level, then the next scene loads.
        {
            print("Reached end of level!");
            SceneManager.LoadScene("Transition");
        }
    }
}
