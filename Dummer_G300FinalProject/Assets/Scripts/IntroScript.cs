﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroScript : MonoBehaviour
{
    //This script handles the text that appears on game start. Stops the game and displays text until player presses a key.
    void Start()
    {
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            Time.timeScale = 1f;
            gameObject.SetActive(false);
        }
    }
}
