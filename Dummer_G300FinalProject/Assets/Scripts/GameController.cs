﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{
    public static GameController gc; //This is accessible from every class and stores important info

    LevelInfo[] levels = {new LevelInfo(7, 4, 3), new LevelInfo(11,4,5), new LevelInfo(12, 4, 3)}; //This array of LevelInfo objects informs what numbers each level allows
    //public Player player;
    public static int currentLevel; //This is both used as a inded for the LevelInfo array and to tell the scenemanager which scene to load
    public int numCollectibles = 3;
    public int numCollected;
    public int par;
    public int jumpCount;
    
    

    void Awake()
    {
       
        setLevel(); 
        print("GameController starts");
        print(currentLevel);
        if(gc == null)
        {
            setLevel();
            gc = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    void setLevel() //sets all the values of the UI to whatever level it is
    {
       
        jumpCount = levels[currentLevel].jumps;
        par = levels[currentLevel].par;
        numCollectibles = levels[currentLevel].collectibles;
        numCollected = 0;
    }
}
